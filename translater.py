import re
import openpyxl
import pandas as pd


TRANSLATIONS = "[ENG_ESP] Teste Raciocínio Adaptativo.xlsx"
QUESTIONS_SHEET = "Texto da Questão "

PT_BR = 1
ES = 2
EN = 3


def load_workbook(name):
    return openpyxl.load_workbook(filename=name)


def get_sheet(workbook, sheetname):
    return workbook.get_sheet_by_name(sheetname)


def read_translations_file():
    wb = load_workbook(name=TRANSLATIONS)
    questions_translations = get_sheet(wb, QUESTIONS_SHEET)
    return questions_translations


def read_questions_file():
    return pd.read_csv("export.csv", index_col="id")


def quotted_text(text):
    return "\"{}\"".format(text)


def find_translation(text, translations, language):
    text = text.strip()
    for row in translations.rows:
        portuguese = row[PT_BR].value
        english = row[EN].value
        spanish = row[ES].value
        if portuguese and (text == portuguese.strip() or quotted_text(text) == portuguese.strip()):
            if language == "en":
                return english.strip()
            if language == "es":
                return spanish.strip()
    print("=========>>>>> COULD NOT FIND TRANSLATION FOR TEXT: {}".format(text))
    return None


class BaseQuestion:
    def __init__(self, question):
        self.question = question


class UtteranceMixin:
    def _translate_utterance(self, text, translations, language):
        utterance_regex = re.compile(r"^([^<]*)<")
        utterance_match = utterance_regex.match(text)
        if utterance_match:
            utterance = utterance_match.group(1).strip()
            translation = find_translation(utterance, translations, language)
            if translation:
                translated_text = re.sub(utterance, translation, text)
                return translated_text
        return None


class DiagramQuestion(UtteranceMixin, BaseQuestion):
    def get_translation(self, translations, language):
        translation = self._translate_items(translations, language)
        translation = self._translate_utterance(translation, translations, language)

        return translation

    def _translate_items(self, translations, language):
        items, translations = self._find_items_translations(translations, language)
        text = self.question["question"]
        if translations:
            for index, item in enumerate(items):
                if translations[index]:
                    text = re.sub(item, translations[index].strip(), text)
        return text

    def _find_items_translations(self, translations, language):
        text = self.question["question"]
        item_regex = re.compile(r'<div class="item">([^<]*)<\/div>')
        parsed_items = item_regex.finditer(text)
        items = []
        items_text = ""
        for item in parsed_items:
            item_formatted = item.group(1).strip()
            items.append(item_formatted)
            items_text += item_formatted + ", "
        items_text = re.sub(r",\s$", "", items_text)

        translation = find_translation(items_text, translations, language)
        parsed_translation = []
        if translation:
            parsed_translation = translation.split(",")
        else:
            for item in items:
                translated_item = find_translation(item, translations, language)
                if translated_item:
                    parsed_translation.append(translated_item)
                else:
                    parsed_translation.append(None)

        return items, parsed_translation


class MatrixQuestion(UtteranceMixin, BaseQuestion):
    def get_translation(self, translations, language):
        translation = self.question["question"]
        translation = self._translate_utterance(translation, translations, language)
        return translation


class SentenceQuestion(UtteranceMixin, BaseQuestion):
    def get_translation(self, translations, language):
        translation = self._translate_sentences(translations, language)
        translation = self._translate_utterance(translation, translations, language)

        return translation

    def _translate_sentences(self, translations, language):
        items, translations = self._find_sentence_translation(translations, language)
#        print("ITEMS: {}, TRANSLATIONS: {}".format(items, translations))
        text = self.question["question"]
        if translations:
            for index, item in enumerate(items):
                if translations[index]:
#                    print("TRANSLALTE: {} TO => {}".format(item, translations[index]))
                    text = re.sub(item, translations[index], text)
#        print(text)
        return text

    def _find_sentence_translation(self, translations, language):
        text = self.question["question"]
        sentence_regex = re.compile(r'<p class="sentence">([^<]*)<\/p>')
        sentences_matches = sentence_regex.finditer(text)
        sentences_translations = []
        sentences = []
        text_to_search = ""
        for sentence in sentences_matches:
            sentence_text = sentence.group(1)
            sentence_text = re.sub(r"^[\W\s]*", "", sentence_text).strip()
            sentences.append(sentence_text)
            text_to_search += sentence_text + "\n\n"

        text_to_search = text_to_search.strip()
        translation = find_translation(text_to_search, translations, language)
        if translation:
            parsed_translations = translation.split("\n\n")
            sentences_translations.extend(parsed_translations)
        else:
            for sentence in sentences:
                translation = find_translation(sentence, translations, language)
                if translation:
                    sentences_translations.append(translation)
                else:
                    print("==============>>>> COULD NOT FIND INDIVIDUAL TRANSLATION FOR: {}".format(sentence))
                    sentences_translations.append(sentence)

        return sentences, sentences_translations


class LetterQuestion(BaseQuestion):
    def get_translation(self, translations, language):
        return find_translation(self.question["question"], translations, language)


class FitInQuestion(BaseQuestion):
    def get_translation(self, translations, language):
        return None


class GenericQuestion(BaseQuestion):
    def get_translation(self, translations, language):
        return find_translation(self.question["question"], translations, language)


class QuestionClassifier:
    @classmethod
    def classify(cls, question):
        if 'class="diagram-items"' in question["question"]:
            return DiagramQuestion(question)
        if 'class="matrix"' in question["question"]:
            return MatrixQuestion(question)
        if 'class="sentence"' in question["question"]:
            return SentenceQuestion(question)
        if "conjunto de letras" in question["question"]:
            return LetterQuestion(question)
        if "Qual das opções a seguir não caberia" in question["question"]:
            return FitInQuestion(question)
        return GenericQuestion(question)


class Translator:
    def __init__(self, questions, translations, language):
        self.questions = questions
        self.translations = translations
        self.language = language

    def translate(self):
        print("Translating files")
        # for idx, question in self.questions.head(5).iterrows():
        for idx, question in self.questions.iterrows():
            translated = (
                QuestionClassifier()
                .classify(question)
                .get_translation(self.translations, self.language)
            )
            if translated:
                self.questions.at[idx, "question"] = translated


def main():
    language = "en"
    translations = read_translations_file()
    questions = read_questions_file()
    Translator(questions, translations, language).translate()
    questions.to_csv("translated_{}.csv".format(language))


if __name__ == "__main__":
    main()
